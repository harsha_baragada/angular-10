import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.css'],
})
export class BookCardComponent implements OnInit {
  @Input() book: any;
  @Output() selectedBook = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  selectBook() {
    this.selectedBook.emit(this.book.id);
  }
}
