import { UserService } from './../service/user.service';
import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css'],
})
export class BooksComponent
  implements
    OnInit,
    OnChanges,
    DoCheck,
    AfterContentInit,
    AfterContentChecked,
    AfterViewInit,
    AfterViewChecked,
    OnDestroy
{
  books: any[] = [];
  constructor(private readonly _service: UserService) {
    console.log('This is a constructor');
  }

  ngOnInit(): void {
    console.log('This is a ngOnInIt');

    this._service.getPosts().subscribe((books) => {
      this.books = books;
    });
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges has been triggered' + changes);
  }

  ngDoCheck(): void {
    console.log('ngDoCheck has been triggered');
  }
  ngAfterContentInit() {
    console.log('This is ngAfterContentInit()');
  }
  ngAfterContentChecked() {
    console.log('ngAfterContentChecked()');
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit()');
  }

  ngAfterViewChecked() {
    console.log('ngAfterViewChecked()');
  }
  bookSelected(event: any) {
    alert('the book ' + event + ' has been selected');
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy()');
  }
}
