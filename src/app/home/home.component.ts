import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  title: string = 'Welcome to our home component';
  isTitleOn!: boolean;

  names: string[] = [
    'Ayesha',
    'Shasmita',
    'Sumitra',
    'Abhirami',
    'Mrudhula',
    'Sushmitha',
  ];
  constructor() {}

  ngOnInit(): void {}

  toggleTitle() {
    this.isTitleOn = !this.isTitleOn;
  }
}
