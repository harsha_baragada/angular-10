import { UserService } from '../service/user.service';
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

@Component({
  selector: 'app-part',
  templateUrl: './part.component.html',
  styleUrls: ['./part.component.css'],
})
export class PartComponent implements OnInit {
  signUpForm!: FormGroup;
  matcher = new MyErrorStateMatcher();
  isSignupSuccessful!: boolean;
  constructor(private readonly userService: UserService) {}

  ngOnInit(): void {
    this.signUpForm = this.createSignUpForm();
  }

  createSignUpForm(): FormGroup {
    return new FormGroup({
      emailFormControl: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      passwordFormControl: new FormControl('', [Validators.required]),
    });
  }
  signUp() {
    if (this.signUpForm.valid) {
      let user = {
        emailId: this.signUpForm.value.emailFormControl,
        pwd: this.signUpForm.value.passwordFormControl,
      };
      this.userService.signUpUser(user).subscribe((response) => {
        console.log(response);
        if (response.body === 201) {
          this.isSignupSuccessful = true;
        }
      });
    }
  }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}
