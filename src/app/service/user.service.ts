import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {
  constructor(private readonly http: HttpClient) {}

  getPosts():Observable<any>{
    return this.http.get('https://jsonplaceholder.typicode.com/posts');
  }

  signUpUser(user: any): Observable<any> {
    let body = JSON.stringify(user);

    return this.http.post('http://localhost:5000/skicco/userSignup', body, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      observe: 'response',
    });
  }
}
